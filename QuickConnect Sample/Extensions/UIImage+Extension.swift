//
//  UIImage+Extension.swift
//  QuickConnect Sample
//
//  Created by Anthony Soulier on 08/04/2021.
//

import Foundation
import UIKit

extension UIImage {
    
    static func color(
        _ color: UIColor,
        size: CGSize = .init(width: 1, height: 1)) -> UIImage {
        UIGraphicsImageRenderer(size: size).image { rendererContext in
            color.setFill()
            rendererContext.fill(CGRect(origin: .zero, size: size))
        }
    }
}
