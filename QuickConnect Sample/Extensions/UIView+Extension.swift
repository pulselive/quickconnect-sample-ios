//
//  UIView+Extension.swift
//  QuickConnect Sample
//
//  Created by Anthony Soulier on 08/04/2021.
//

import Foundation
import UIKit

extension UIView {
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get { layer.cornerRadius }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
}
