//
//  UIButton+Extension.swift
//  QuickConnect Sample
//
//  Created by Anthony Soulier on 08/04/2021.
//

import Foundation
import UIKit

extension UIButton {
    func setBackgroundColor(_ color: UIColor?, for state: UIControl.State) {
        setBackgroundImage(color.map { UIImage.color($0) }, for: state)
    }
}
