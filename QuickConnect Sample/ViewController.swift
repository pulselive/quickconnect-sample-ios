//
//  ViewController.swift
//  QuickConnect Sample
//
//  Created by Anthony Soulier on 07/04/2021.
//

import UIKit
import QuickConnect
import PLFanData
import PLFanDataCommon

class ViewController: UIViewController {
    
    @IBOutlet private var startButton: UIButton?

    private let quickConnect = QuickConnectModule(
        // Use custom font, e.g SFProText family
        fontProvider: FontProvider(regularFamily: "SFProText")
    )
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    private func setup() {
        startButton?.setBackgroundColor(UIColor(named: "AccentColor"), for: .normal)
        startButton?.tintColor = .white
    }

    @IBAction private func startQuickConnect() {
        quickConnect.start(owner: self)
    }
    
}

